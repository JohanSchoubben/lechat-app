package be.johancodes.model;

import javax.persistence.*;
import java.util.*;

@Entity
@Table(name = "users", schema = "testing")
@org.hibernate.annotations.NamedQuery(name = "User.findByCredentials",
                    query = "select u from User u where u.username = :username and u.password = :password")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false, unique = true)
    private Integer id;
    @Column(length = 32, nullable = false, unique = true)
    private String username;
    @Column(length = 50, nullable = false, unique = false)
    private String email;
    @Column(length = 200, nullable = false, unique = false)
    private String password;
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(name = "friendships",
            joinColumns = @JoinColumn(name = "this_user_id"), foreignKey = @ForeignKey(name = "FK_this_user_id"),
            inverseJoinColumns = @JoinColumn(name = "is_friend_with", foreignKey = @ForeignKey(name = "FK_is_friend_with")))
    List<User> listOfFriends = new ArrayList<User>();
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    @JoinTable(name = "requests",
            joinColumns = @JoinColumn(name = "sender_id"), foreignKey = @ForeignKey(name = "FK_request_sender_id"),
            inverseJoinColumns = @JoinColumn(name = "receiver_id", foreignKey = @ForeignKey(name = "FK_request_receiver_id")))
    List<User> listOfRequests = new ArrayList<User>();

    public User() {
    }

    public User(String username, String email, String password) {
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public List<User> getListOfFriends() {
        return listOfFriends;
    }

    public void setListOfFriends(List<User> listOfFriends) {
        this.listOfFriends = listOfFriends;
    }

    public List<User> getListOfRequests() {
        return listOfRequests;
    }

    public void setListOfRequests(List<User> listOfRequests) {
        this.listOfRequests = listOfRequests;
    }

    @Override
    public String toString() {
        return getUsername() + " (id " + getId() + ")";
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId().intValue());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        User that = (User) obj;
        return Integer.compare(that.getId(), getId()) == 0;
    }
}
