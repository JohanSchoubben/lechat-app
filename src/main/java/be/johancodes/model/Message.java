package be.johancodes.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "messages", schema = "testing")
public class Message {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "message_id", nullable = false, unique = true)
    private Integer id;
    @ManyToOne
    @JoinColumn(name = "sender_id")
    private User sender;
    @ManyToOne
    @JoinColumn(name = "receiver_id")
    private User receiver;
    @Column(name = "timestamp", nullable = false)
    private LocalDateTime timestamp;
    @Column(length = 300)
    private String text;

    public Message() {
    }

    public Message(User sender, User receiver, LocalDateTime timestamp, String text) {
        this.sender = sender;
        this.receiver = receiver;
        this.timestamp = timestamp;
        this.text = text;
    }

    public Integer getId() {
        return id;
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public User getReceiver() {
        return receiver;
    }

    public void setReceiver(User receiver) {
        this.receiver = receiver;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "From: " + sender.getUsername() + "\n" + "To: " + receiver.getUsername() + "\n" +
                "Date: " + getTimestamp() + "\n" + "Text: " + getText();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId().intValue());
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        Message that = (Message) obj;
        return Integer.compare(that.getId(), getId()) == 0;
    }
}
