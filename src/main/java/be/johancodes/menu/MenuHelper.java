package be.johancodes.menu;

import be.johancodes.model.Message;
import be.johancodes.model.User;

import java.util.List;
import java.util.Locale;

import static be.johancodes.utilities.MenuUtility.thickLine;
import static be.johancodes.utilities.MenuUtility.center;
import static be.johancodes.utilities.MenuUtility.thinLine;

/**
 * Provides helper methods for AppService
 */

public class MenuHelper {
    /**
     * Use thin line for styling
     */
    public static void displayMenuHeader(String header) {
        System.out.println(thinLine());
        System.out.println(center(header.toUpperCase(Locale.ROOT)));
        System.out.println(thinLine());
    }

    /**
     * Use thick line for styling
     */
    public static void displayMenuTitle(String title) {
        System.out.println(thickLine());
        System.out.println(center(title.toUpperCase(Locale.ROOT)));
        System.out.println(thickLine());
    }

    /**
     * Use singular for size 1, use plural for other values
     */
    public static void numberOfUsersInList(List<User> list, String listName) {
        if ((list.size()) == 1) {
            StringBuilder sb = new StringBuilder(listName);
            System.out.println("You have " + list.size() + " " + (sb.deleteCharAt(sb.length() - 1)).toString());
        } else {
            System.out.println("You have " + list.size() + " " + listName);
        }
    }

    /**
     * Print out timestamp and receiver for list of sent messages
     */
    public static void printSentMessagesInfo(List<Message> messages) {
        if (messages.size() > 0) {
            System.out.printf("\tSend date\t\t\t\t\tTo user%n");
            for (int i = 0; i < messages.size(); i++) {
                System.out.printf((i + 1) + ".\t" + messages.get(i).getTimestamp() + "\t" + messages.get(i).getReceiver().toString() + "%n");
            }
        } else {
            System.out.println("No messages");
        }
    }

    /**
     * Print out timestamp and sender for list of received messages
     */
    public static void printReceivedMessagesInfo(List<Message> messages) {
        if (messages.size() > 0) {
            System.out.printf("\tReceive date\t\t\t\tFrom user%n");
            for (int i = 0; i < messages.size(); i++) {
                System.out.printf((i + 1) + ".\t" + messages.get(i).getTimestamp() + "\t" + messages.get(i).getSender().toString() + "%n");
            }
        } else {
            System.out.println("No messages");
        }
    }
}
