package be.johancodes.menu;

import be.johancodes.model.Message;
import be.johancodes.model.User;
import be.johancodes.service.*;
import java.util.*;
import static be.johancodes.utilities.KeyboardUtility.*;
import static be.johancodes.utilities.MenuUtility.*;


public class AppService {
    int choice;
    boolean keepGoing;
    boolean isPresent;
    User sender;
    User receiver;
    /**
     * Program to interface, not to implementation
     */
    MessageService msi = ServiceFactory.getMessageService();
    UserService usi = ServiceFactory.getUserService();
    public void launchApp() {
        displayIntro();
        displayMainMenu();
    }

    //region intro
    public void displayIntro() {
        System.out.println(thickLine());
        System.out.println(center("LeChat"));
        System.out.println(center("Messenger App"));
        System.out.println(thickLine());
        System.out.println(center("by"));
        System.out.println(center("Johan Schoubben"));
        System.out.println(thickLine());
        System.out.println();
    }
    //endregion

    public void displayMainMenu() {
        MenuHelper.displayMenuHeader("Main menu");
        choice = askForChoice(MenuOptions.getMainMenuOptions());
        mainMenuChoices(choice);
    }

    public void displayUserMenu() {
        sender = usi.getSender();
        MenuHelper.displayMenuTitle("Welcome, " + sender.getUsername());
        choice = askForChoice(MenuOptions.getUserMenuOptions());
        userMenuChoices(choice);
    }

    //region main menu
    public void mainMenuChoices(int choice) {
        switch (choice) {
            case 0:
                usi.registerNewUser();
                keepGoing = askYOrN("Do you want to log in? ");
                if (keepGoing) {
//                    sender = usi.getSender();
                    displayUserMenu();
                } else {
//                    sender = new User();
                    displayMainMenu();
                }
                break;
            case 1:
                usi.logInUser();
                sender = usi.getSender();
                if (sender != null) { // TODO avoid check and continue if user not found
                    displayUserMenu();
                    sender = sender;
                } else {
                    displayMainMenu();
                }
                break;
            case 2:
                usi.closeConnection();
                System.exit(0);
                break;
        }
    }
    //endregion

    //region user menu
    public void userMenuChoices(int choice) {
        switch (choice) {
            case 0:
                MenuHelper.displayMenuHeader("Available users for messages");
                usi.viewFriendships().forEach(System.out::println);
                msi.sendMessage();
                displayUserMenu();
                break;
            case 1:
                MenuHelper.displayMenuHeader("Sent messages");
                List<Message> sentMessages = msi.viewSentMessages();
                MenuHelper.printSentMessagesInfo(sentMessages); // TODO select and read message
                displayUserMenu();
                break;
            case 2:
                MenuHelper.displayMenuHeader("Received messages");
                List<Message> receivedMessages = msi.viewReceivedMessages();
                MenuHelper.printReceivedMessagesInfo(receivedMessages); // TODO select and read message
                displayUserMenu();
                break;
            case 3:
                MenuHelper.displayMenuHeader("Available users for requests");
                List<User> availableUsers = usi.viewAvailableUsers();
                availableUsers.forEach(System.out::println);
                usi.sendRequest();
                availableUsers.clear();
                displayUserMenu();
                break;
            case 4:
                MenuHelper.displayMenuHeader("Received friendship requests"); // TODO loop if requests > 1
                List<User> requests = usi.viewRequests();
                MenuHelper.numberOfUsersInList(requests, "requests");
                requests.forEach(System.out::println);
                if ((requests.size()) == 0) {
                    displayUserMenu();
                } else {
                    keepGoing = askYOrN("Do you wish to accept a request? ");
                    if (keepGoing) {
                        usi.addFriend();
                        displayUserMenu(); // TODO not while in loop
                    } else {
                        displayUserMenu();
                    }
                }
                break;
            case 5:
                MenuHelper.displayMenuHeader("Your friendships");
                List<User> friendships = usi.viewFriendships();
                MenuHelper.numberOfUsersInList(friendships, "friendships");
                friendships.forEach(System.out::println);
                displayUserMenu();
                break;
            case 6:
                MenuHelper.displayMenuHeader("Available users"); // TODO update list 'available' after accepting request
                availableUsers = usi.viewAvailableUsers();
                availableUsers.forEach(System.out::println);
                availableUsers.clear();
                displayUserMenu();
                break;
            case 7:
                usi.logOutUser();
                displayMainMenu();
                break;
        }
        //endregion
    }
}

