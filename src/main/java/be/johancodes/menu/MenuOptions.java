package be.johancodes.menu;

public class MenuOptions {
    private static String[] mainMenuOptions =  {
            "Register",
            "Log in",
            "Quit"
    };
    private static String[] userMenuOptions = {
            "Send message",
            "View sent messages",
            "View received messages",
            "Send request",
            "View requests",
            "View friends",
            "View available users",
            "Log out"
    };

    public static String[] getMainMenuOptions() {
        return mainMenuOptions;
    }

    public static String[] getUserMenuOptions() {
        return userMenuOptions;
    }
}
