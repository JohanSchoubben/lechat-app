package be.johancodes.service;

import be.johancodes.model.Message;
import be.johancodes.model.User;
import be.johancodes.repository.MessageRepository;
import be.johancodes.repository.MessageRepositoryImplementation;
import java.time.LocalDateTime;
import java.util.List;

import static be.johancodes.utilities.KeyboardUtility.*;
import static be.johancodes.repository.EntityManagerFactorySingleton.*;

public class MessageServiceImplementation implements MessageService {

    MessageRepository mri = new MessageRepositoryImplementation(getEntityManager());
    UserService usi = ServiceFactory.getUserService();
    Integer id;
    User sender;
    User receiver;
    Message message = new Message();

    @Override
    public Message sendMessage() {
        sender = usi.getSender();
        receiver = usi.getReceiver();
        message.setSender(sender);
        message.setReceiver(receiver);
        message.setTimestamp(LocalDateTime.now());
        message.setText(ask("Enter a message"));
        mri.saveMessage(message);
        System.out.println("Message sent to " + receiver.getUsername());
        return message;
    }

    @Override
    public List<Message> viewSentMessages() {
        id = usi.getSender().getId();
        List<Message> sentMessages = mri.findMessagesBySenderId(id);
        return sentMessages;
    }

    @Override
    public List<Message> viewReceivedMessages() {
        id = usi.getSender().getId();
        return mri.findMessagesByReceiverId(id);
    }
}
