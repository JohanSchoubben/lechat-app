package be.johancodes.service;

import be.johancodes.model.User;
import static be.johancodes.repository.EntityManagerFactorySingleton.closeEntityManager;
import static be.johancodes.repository.EntityManagerFactorySingleton.getEntityManager;
import be.johancodes.repository.*;
import static be.johancodes.utilities.KeyboardUtility.*;
import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class UserServiceImplementation implements UserService {

    UserRepository uri = new UserRepositoryImplementation(getEntityManager());
    User sender;
    User receiver;
    boolean isPresent;
    boolean isConfirmed;

    @Override
    public User getSender() {
        return sender;
    }

    @Override
    public User getReceiver() {
        Integer id = askForInt("Enter receiver_id");
        receiver = uri.findUserById(id);
        return receiver;
    }

    @Override
    public User registerNewUser() {
        verifyUsername();
        sender.setEmail(askForEmail("Enter your e-mail: "));
        verifyPassword();
        uri.saveUser(sender);
        return sender;
    }

    private void verifyUsername() {
        do {
            String username = ask("Enter a username: ");
            if (usernameTaken(username)) {
                System.out.println("Username already exists");
            } else {
            sender.setUsername(username);
            }
        } while (isPresent);
    }

    private void verifyPassword() { // TODO check for length & characters
        do {
            String password = ask("Enter a password: ");
            String passwordRepeat = ask("Confirm this password: ");
            if (password.equals(passwordRepeat)) {
                System.out.println("Password confirmed");
                isConfirmed = true;
                sender.setPassword(password);
            } else {
                System.out.println("Passwords do not match");
            }
        } while (!isConfirmed);
    }

    @Override
    public User logInUser() {
        do {
            try {
                String username = ask("Enter username: ");
                String password = ask("Enter password: ");
                if (userInDb(username, password)) {
                    sender = uri.findUserByCredentials(username, password);
                } else {
                    isPresent = false;
                    System.out.println("No such user found");
//                    uri.clearEntityManager();
                }
            } catch (NoResultException noResultException) {
                System.out.println(noResultException.getMessage());
            }
        } while (!isPresent);
        return sender;
    }

    public boolean userInDb(String username, String password) {
        List<User> allUsers = uri.findAllUsers();
        isPresent = allUsers.stream().anyMatch(u ->
                (u.getUsername()).equals(username) && (u.getPassword()).equals(password));
        return isPresent;
    }

    public boolean usernameTaken(String username) {
        List<User> allUsers = uri.findAllUsers();
        isPresent = allUsers.stream().anyMatch(u ->
                (u.getUsername()).equals(username));
        return isPresent;
    }

    @Override
    public User sendRequest() {
        sender = getSender();
        Integer id = askForInt("Enter receiver_id");
        receiver = uri.findUserById(id);
        receiver.getListOfRequests().add(sender);
        uri.saveUser(receiver);
        System.out.println("Request sent to " + receiver);
        return sender;
    }

    @Override
    public List<User> viewRequests() {
        sender = getSender();
        return sender.getListOfRequests();
    }

    @Override
    public List<User> viewAvailableUsers() {
        List<User> allUsers = new ArrayList<>();
        allUsers = uri.findAllUsers();
        allUsers.remove(sender);
        List<User> requests = sender.getListOfRequests();
        List<User> friendships = sender.getListOfFriends();
//        List<User> availableUsers = new ArrayList<>(allUsers);
//        availableUsers.removeAll(requests);
//        availableUsers.removeAll(friendships);
//        return availableUsers;
        return allUsers.stream()
                .filter(u -> !requests.contains(u) && !friendships.contains(u))
                .collect(Collectors.toList());
    }

    @Override
    public User addFriend() {
        receiver = getReceiver();
        sender.getListOfFriends().add(receiver);
        receiver.getListOfFriends().add(sender);
        sender.getListOfRequests().remove(receiver);
        System.out.println("Request from " + receiver + " accepted");
        uri.saveUser(sender);
        uri.saveUser(receiver);
        return sender;
    }

    @Override
    public List<User> viewFriendships() {
        sender = getSender();
        return sender.getListOfFriends();
    }

    @Override
    public User removeFriend() {
        return null;
    }



    @Override
    public void logOutUser() {
        sender = new User();
        uri.clearEntityManager();
    }

    @Override
    public void closeConnection() {
        closeEntityManager();
    }

}
