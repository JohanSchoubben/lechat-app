package be.johancodes.service;

public interface AdminService {
    void showAllRequests();
    void showAllFriendships();
    void showRequestsByUserId();
}
