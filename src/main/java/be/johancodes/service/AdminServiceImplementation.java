package be.johancodes.service;

import be.johancodes.menu.MenuHelper;
import be.johancodes.model.User;
import be.johancodes.repository.UserRepository;
import be.johancodes.repository.UserRepositoryImplementation;
import javax.persistence.EntityManager;
import java.util.*;
import static be.johancodes.repository.EntityManagerFactorySingleton.*;

public class AdminServiceImplementation implements AdminService {

    public AdminServiceImplementation(EntityManager entityManager){}

    UserRepository uri = new UserRepositoryImplementation(getEntityManager());

    @Override
    public void showAllRequests() {
        List<User> userList = uri.findAllUsers();
        MenuHelper.displayMenuTitle("All requests");
        for (User user : userList) {
            MenuHelper.displayMenuHeader("Request(s) for " + user.getUsername());
            (user.getListOfRequests()).forEach(System.out::println);
        }
    }

    @Override
    public void showAllFriendships() {
        List<User> userList = uri.findAllUsers();
        MenuHelper.displayMenuTitle("All friendships");
        for (User user : userList) {
            MenuHelper.displayMenuHeader("Friendships of " + user.getUsername());
//            System.out.println(MenuUtility.thinLine());
//            System.out.println(("Friendships of " + user.getUsername().toUpperCase(Locale.ROOT)));
//            System.out.println(MenuUtility.thinLine());
            (user.getListOfFriends()).forEach(System.out::println);
        }
    }

    public void showRequestsByUserId() {
        int[] ids = {1, 2, 3, 5};
        List<User> users = new ArrayList<>();
        for (int id : ids) { users.add(uri.findUserById(id)); }
        for (User user : users) {
            MenuHelper.displayMenuHeader("Requests for " + user.getUsername());
            (user.getListOfRequests()).forEach(System.out::println);
        }
    }
}
