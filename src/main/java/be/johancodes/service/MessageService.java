package be.johancodes.service;

import be.johancodes.model.Message;
import java.util.List;

public interface MessageService {
    Message sendMessage();
    List<Message> viewSentMessages();
    List<Message> viewReceivedMessages();
}
