package be.johancodes.service;

public class ServiceFactory {
    public static UserService userService = new UserServiceImplementation();
    public static MessageService messageService = new MessageServiceImplementation();

    public static UserService getUserService() {
        return userService;
    }

    public static MessageService getMessageService() {
        return messageService;
    }
}
