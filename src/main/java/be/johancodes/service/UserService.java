package be.johancodes.service;

import be.johancodes.model.User;

import java.util.List;

public interface UserService {
    User registerNewUser();
    User logInUser();
    void logOutUser();
    void closeConnection();
    User sendRequest();
    List<User> viewRequests();
    List<User> viewFriendships();
    List<User> viewAvailableUsers();
    User addFriend();
    User removeFriend(); // TODO implement
    User getSender();
    User getReceiver();
}
