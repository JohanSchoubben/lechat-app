package be.johancodes.repository;

import be.johancodes.model.User;
import javax.persistence.*;
import java.util.*;

public class UserRepositoryImplementation implements UserRepository {

    private final EntityManager entityManager;

    public UserRepositoryImplementation(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public User findUserById(Integer id) {
        return entityManager.find(User.class, id);
    }

    @Override
    public User findUserByCredentials(String username, String password) {
        return entityManager.createNamedQuery("User.findByCredentials", User.class)
                .setParameter("username", username)
                .setParameter("password", password)
                .getSingleResult();
    }

    @Override
    public User saveUser(User user) {
            entityManager.getTransaction().begin();
            entityManager.persist(user);
            entityManager.getTransaction().commit();
        return user;
    }

    @Override
    public List<User> findAllUsers() {
        return entityManager.createQuery("select u from User u")
                .getResultList();
    }

    @Override
    public void deleteUserById(Integer id) {
        User user = entityManager.find(User.class, id);
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(user);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<User> findRequests(Integer id) {
        return entityManager.createQuery(
                "select r from User r where r.id = :receiver_id")
                .setParameter("receiver_id", id)
                .getResultList();
    }

    @Override
    public void clearEntityManager() {
        entityManager.clear();
    }
}
