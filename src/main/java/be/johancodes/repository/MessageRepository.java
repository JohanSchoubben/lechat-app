package be.johancodes.repository;

import be.johancodes.model.Message;
import java.util.*;

public interface MessageRepository {
    Message saveMessage(Message message);
    List<Message> findMessagesBySenderId(Integer senderId);
    List<Message> findMessagesByReceiverId(Integer receiverId);
    Message findMessageById(Integer id);
    List<Message> findAllMessages();
    void deleteMessageById(Integer id);
}
