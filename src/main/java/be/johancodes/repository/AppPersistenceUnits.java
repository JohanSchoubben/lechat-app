package be.johancodes.repository;

public enum AppPersistenceUnits {

    LECHAT("lechatPersistenceUnit");

    private final String label;

    private AppPersistenceUnits(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }
}
