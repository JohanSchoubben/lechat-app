package be.johancodes.repository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import static be.johancodes.repository.AppPersistenceUnits.*;

public class EntityManagerFactorySingleton {

    public static final EntityManagerFactory entityManagerFactory =
            Persistence.createEntityManagerFactory(LECHAT.getLabel());
    public static final EntityManager entityManager = entityManagerFactory.createEntityManager();

    public static EntityManager getEntityManager() {
        return entityManager;
    }

    public static void closeEntityManager() {
        entityManager.close();
        entityManagerFactory.close();
    }
}
