package be.johancodes.repository;

import be.johancodes.model.User;
import java.util.*;

public interface UserRepository {
    User saveUser(User user);
    User findUserByCredentials(String username, String password);
    User findUserById(Integer id);
    List<User> findAllUsers();
    List<User> findRequests(Integer id);
    void deleteUserById(Integer id);
    void clearEntityManager();
}
