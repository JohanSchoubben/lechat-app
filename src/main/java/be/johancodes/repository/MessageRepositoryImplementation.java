package be.johancodes.repository;

import be.johancodes.model.Message;
import javax.persistence.*;
import java.util.*;

public class MessageRepositoryImplementation implements MessageRepository {

    private EntityManager entityManager;

    public MessageRepositoryImplementation(EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @Override
    public Message saveMessage(Message message) {
            entityManager.getTransaction().begin();
            entityManager.persist(message);
            entityManager.getTransaction().commit();
            return message;
    }

    @Override
    public List<Message> findMessagesBySenderId(Integer senderId) {
        List<Message> messagesByUserId =
                entityManager.createQuery("select m from Message m where m.sender.id = :senderId")
                        .setParameter("senderId", senderId)
                        .getResultList();
        return messagesByUserId;
    }

    @Override
    public List<Message> findMessagesByReceiverId(Integer receiverId) {
        return entityManager.createQuery("select m from Message m where m.receiver.id = :receiverId")
                .setParameter("receiverId", receiverId)
                .getResultList();
    }

    @Override
    public Message findMessageById(Integer id) {
        Message message = entityManager.find(Message.class, id);
        return message;

    }

    @Override
    public List<Message> findAllMessages() {
        return entityManager.createQuery("select m from Message m").getResultList();
    }

    @Override
    public void deleteMessageById(Integer id) {
        Message message = entityManager.find(Message.class, id);
        try {
            entityManager.getTransaction().begin();
            entityManager.remove(message);
            entityManager.getTransaction().commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
